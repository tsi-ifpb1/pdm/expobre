package com.example.expobre

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var tvNumeros: TextView
    private lateinit var bAtualizar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.tvNumeros = findViewById(R.id.tvNumeros)
        this.tvNumeros.text = MegaSena.getInstance().joinToString(" ")

        this.bAtualizar = findViewById(R.id.bAtualizar);
        bAtualizar.setOnClickListener() {
            this.tvNumeros.text = MegaSena.getInstance().joinToString(" ")
        };
    }
}
